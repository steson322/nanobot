//#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/photo/cuda.hpp>

#include "vehicle.h"

using namespace cv;
using namespace std;

void waitForExit()
{
  std::cout << "Please press [Enter] to exit." << std::endl;
  std::cin.get();
}

int main()
{
  nanobot::core::Vehicle vehicle;

  vehicle.start();

  while (true)
  {
    vehicle.printState();
    // char key = vision.displayMotion("Motion", 100);
    // if (key == 'q')
    // {
    //   break;
    // }
    usleep(400 * 1000);
  }

  waitForExit();

  vehicle.stop();

  return 0;
}