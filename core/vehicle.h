#ifndef CORE_VEHICLE
#define CORE_VEHICLE

#include <iostream>
#include <memory>
#include <eigen3/Eigen/Dense>
#include <opencv2/opencv.hpp>
#include "vision/vision.h"
#include "sensor/sensor.h"

namespace nanobot::core
{
  class Vehicle
  {
  private:
    std::shared_ptr<nanobot::sensor::Sensor> _sensor;
    std::shared_ptr<nanobot::vision::Vision> _vision;
    
    cv::Mat _tran_mat = cv::Mat(3, 3, CV_64F, new double[3][3]{{0, 0, 1}, {1, 0, 0}, {0, 1, 0}});
    
    // Visual odometry states
    cv::Mat _position_vo;
    cv::Mat _velocity_vo;
    cv::Mat _pose_vo;

    void handleVision(const cv::Mat &rmat, const cv::Mat &tvec);
  public:
    Vehicle();
    void start();
    void stop();
    void printState();
  };
}; // namespace nanobot::core

#endif /* !CORE_VEHICLE */