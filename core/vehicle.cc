#include "vehicle.h"
#include <opencv2/core/eigen.hpp>

#define ENABLE_VISUAL_ODOMETRY false

nanobot::core::Vehicle::Vehicle()
{
  _sensor = std::make_shared<nanobot::sensor::Sensor>(50);

  nanobot::vision::MotionCallback motion_fn = [this](const cv::Mat &rmat, const cv::Mat &tvec) {
    if (ENABLE_VISUAL_ODOMETRY)
    {
      this->handleVision(rmat, tvec);
    }
  };

  _vision = std::make_shared<nanobot::vision::Vision>(100, cv::Size(640, 360), motion_fn);
}

void nanobot::core::Vehicle::handleVision(const cv::Mat &rmat, const cv::Mat &tvec)
{
  // Capture latest state.
  Eigen::Vector3d position, velocity;
  Eigen::Matrix3d pose;
  this->_sensor->getState(position, velocity, pose);

  if (_pose_vo.empty())
  {
    cv::eigen2cv(position, _position_vo);
    cv::eigen2cv(velocity, _velocity_vo);
    cv::eigen2cv(pose, _pose_vo);
    return;
  }

  cv::Mat position_est_cv = _position_vo - (_tran_mat.t() * _pose_vo).t() * tvec;
  cv::Mat velocity_est_cv = position_est_cv - _position_vo; // * delta_t
  cv::Mat pose_est_mat_cv = _tran_mat * rmat.t() * _tran_mat.t() * _pose_vo;
  cv::Mat pose_est_cv;
  cv::Rodrigues(pose_est_mat_cv, pose_est_cv);

  Eigen::Vector3d position_est;
  Eigen::Vector3d velocity_est;
  Eigen::Vector3d pose_est;
  cv::cv2eigen(position_est_cv, position_est);
  cv::cv2eigen(velocity_est_cv, velocity_est);
  cv::cv2eigen(pose_est_cv, pose_est);

  Eigen::VectorXd var_vec(6);
  var_vec(0) = var_vec(1) = var_vec(2) = 0.001;
  var_vec(3) = var_vec(4) = var_vec(5) = 0.001;

  this->_sensor->estimateByVision(position_est, velocity_est, pose_est, var_vec.asDiagonal());

  // Record state.
  cv::eigen2cv(position, _position_vo);
  cv::eigen2cv(velocity, _velocity_vo);
  cv::eigen2cv(pose, _pose_vo);
}

void nanobot::core::Vehicle::start()
{
  _sensor->start();
  _vision->start();
}

void nanobot::core::Vehicle::stop()
{
  _sensor->stop();
  _vision->stop();
}

void nanobot::core::Vehicle::printState()
{
  if (this->_sensor == nullptr)
  {
    return;
  }

  this->_sensor->printState();
}