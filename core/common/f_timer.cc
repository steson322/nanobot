#include <chrono>
#include "f_timer.h"

nanobot::common::FTimer::FTimer(int interval): _interval(interval), _execute(false) {

}

nanobot::common::FTimer::~FTimer() {
  if (_execute.load(std::memory_order_acquire))
  {
    stop();
  };
}

void nanobot::common::FTimer::start(std::function<void(void)> fn) {
  // Stop the thread if running.
  if (_execute.load(std::memory_order_acquire))
  {
    stop();
  };

  // Set running.
  _execute.store(true, std::memory_order_release);
  _thread = std::thread([this, fn]() {
    while (this->_execute.load(std::memory_order_acquire))
    {
      std::chrono::high_resolution_clock::time_point start_time = std::chrono::high_resolution_clock::now();
      // run function
      fn();
      std::chrono::high_resolution_clock::time_point end_time = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
      int usleep = this->_interval * 1000 - duration.count();
      if (usleep > 0) 
      {
        std::this_thread::sleep_for(std::chrono::microseconds(usleep));
      }
    }
  });
}

void nanobot::common::FTimer::stop() {
  _execute.store(false, std::memory_order_release);
  if (_thread.joinable())
  {
    _thread.join();
  }
}