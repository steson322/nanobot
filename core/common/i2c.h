#ifndef COMMON_I2C
#define COMMON_I2C

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>

namespace nanobot::common {
  class I2C {
    private:
      std::string _path;
      int _i2c_bus;
    public:
      I2C(std::string path);
      ~I2C();
      bool init();
      bool init(uint8_t dev_addr, uint8_t reg_addr, uint8_t exp_val);
      bool read(uint8_t dev_addr, uint8_t reg_addr, uint8_t &ret_val);
      bool write(uint8_t dev_addr, uint8_t reg_addr, uint8_t val);
  };
}; // namespace nanobot::common

#endif /* !COMMON_I2C */