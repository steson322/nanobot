#ifndef COMMON_F_TIMER
#define COMMON_F_TIMER

#include <atomic>
#include <functional>
#include <memory>
#include <thread>
#include <stdlib.h>

namespace nanobot::common {
  class FTimer {
    private:
      int _interval;
      std::atomic<bool> _execute;
      std::thread _thread;
    public:
      FTimer(int interval);
      ~FTimer();
      void start(std::function<void(void)> fn);
      void stop();
  };
}; // namespace nanobot::common

#endif /* !COMMON_F_TIMER */