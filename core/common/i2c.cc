#include <iostream>
#include "i2c.h"

nanobot::common::I2C::I2C(std::string path) : _path(path)
{
}

nanobot::common::I2C::~I2C()
{
  if (_i2c_bus > 0) {
    ::close(_i2c_bus);
  }
}

bool nanobot::common::I2C::init()
{
  _i2c_bus = ::open(_path.c_str(), O_RDWR);
  if (_i2c_bus < 0) {
    std::cerr << "Unable to open the I2C bus" << std::endl;
    return false;
  }
  return true;
}

bool nanobot::common::I2C::init(uint8_t dev_addr, uint8_t reg_addr, uint8_t exp_val)
{
  bool succeed = init();
  if (!succeed) {
    return false;
  }

  uint8_t ret_val;
  if (!read(dev_addr, reg_addr, ret_val)) {
    return false;
  }

  return ret_val == exp_val;
}

bool nanobot::common::I2C::read(uint8_t dev_addr, uint8_t reg_addr, uint8_t &ret_val)
{
  if (ioctl(_i2c_bus, I2C_SLAVE, dev_addr) < 0)
  {
    std::cerr << "Unable to communicate over I2C bus" << std::endl;
    return false;
  }
  auto w_sise = ::write(_i2c_bus, &reg_addr, 1);
  auto r_size = ::read(_i2c_bus, &ret_val, 1);
  return true;
}

bool nanobot::common::I2C::write(uint8_t dev_addr, uint8_t reg_addr, uint8_t val)
{
  if (ioctl(_i2c_bus, I2C_SLAVE, dev_addr) < 0)
  {
    std::cerr << "Unable to communicate over I2C bus" << std::endl;
    return false;
  }

  uint8_t buf[2];
  buf[0] = reg_addr;
  buf[1] = val;
  auto w_sise = ::write(_i2c_bus, buf, 2);
  return true;
}