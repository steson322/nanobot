#ifndef VISION_VISION
#define VISION_VISION

#include <atomic>
#include <memory>
#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/core/types.hpp>
#include <opencv2/features2d.hpp>
#include "depth_mapper.h"
#include "motion.h"
#include "../common/f_timer.h"

namespace nanobot::vision
{

  typedef std::function<void(const cv::Mat&, const cv::Mat&)> MotionCallback;

  class Vision
  {
  private:
    bool _calibrated = false;
    std::string _calibration_file;
    cv::Size _image_size;
    std::shared_ptr<common::FTimer> _timer;
    std::shared_ptr<DepthMapper> _depth_mapper;
    std::shared_ptr<Motion> _motion;
    cv::VideoCapture _camera_left;
    cv::VideoCapture _camera_right;
    cv::Mat _camera_matrix_left;
    cv::Mat _dist_coeffs_left;
    cv::Mat _camera_matrix_right;
    cv::Mat _dist_coeffs_right;
    cv::Mat _rectification;
    cv::VideoCapture camera(int id);
    
    cv::cuda::GpuMat _d_frame_left;
    cv::cuda::GpuMat _d_frame_right;
    cv::cuda::GpuMat _d_disparity;
    cv::cuda::GpuMat _d_depth_map;

    cv::Mat _tran_mat = cv::Mat(3, 3, CV_64F, new double[3][3]{{0, 0, 1}, {1, 0, 0}, {0, 1, 0}});

    cv::Mat _position = cv::Mat::zeros(3, 1, CV_64F);
    cv::Mat _pose = cv::Mat::eye(3, 3, CV_64F);
    MotionCallback _motion_fn;
    
    void loadCalibration();
  public:
    Vision(int interval, cv::Size image_size, MotionCallback motion_fn);
    Vision(
        int interval,
        cv::Size image_size,
        MotionCallback motion_fn,
        std::string calibration_file,
        int left_id,
        int right_id);
    void start();
    void stop();
    double calibrate();
    char displayDisparity(std::string name, int delay) const;
    char displayMotion(std::string name, int delay) const;
  };
}; // namespace nanobot::vision

#endif /* !VISION_VISION */