#ifndef VISION_CALIBRATION
#define VISION_CALIBRATION

#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>

namespace nanobot::vision
{
  class Calibration
  {
  private:
    cv::Size image_size;
    cv::Size pattern_size;
    std::vector<cv::Point3f> object_corners;
    float baseline;
    int sample_counts;
    bool display_image;
    bool capture(
      cv::Mat &frame, 
      std::vector<std::vector<cv::Point3f>> &object_points, 
      std::vector<std::vector<cv::Point2f>> &image_points) const;
  public:
    Calibration(cv::Size image_size, cv::Size pattern_size, float pattern_length, float baseline);
    void display(bool display_image);
    double calibrate(cv::VideoCapture &camera, cv::Mat &camera_matrix, cv::Mat &dist_coeffs) const;
    double stereoCalibrate(
      cv::VideoCapture &camera_left, 
      cv::VideoCapture &camera_right, 
      cv::Mat &camera_matrix_left, 
      cv::Mat &dist_coeffs_left, 
      cv::Mat &camera_matrix_right, 
      cv::Mat &dist_coeffs_right,
      cv::Mat &rectification) const;
  };
}; // namespace nanobot::vision

#endif /* !VISION_CALIBRATION */