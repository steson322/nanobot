#ifndef VISION_DEPTH_MAPPER
#define VISION_DEPTH_MAPPER

#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/cudastereo.hpp>

namespace nanobot::vision
{
  class DepthMapper
  {
  private:
    float focal_length;
    float baseline;
    int num_disparities;
    cv::Ptr<cv::cuda::StereoBM> matcher;
    void init();

  public:
    DepthMapper(float focal_length, float baseline);
    DepthMapper(float focal_length, float baseline, int num_disparities);
    void map(
        const cv::cuda::GpuMat &d_frame_left_gray,
        const cv::cuda::GpuMat &d_frame_right_gray,
        cv::cuda::GpuMat &d_disparity,
        cv::cuda::GpuMat &d_depth_map,
        cv::cuda::Stream &stream) const;
  };
}; // namespace nanobot::vision

#endif /* !VISION_DEPTH_MAPPER */