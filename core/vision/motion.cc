#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <opencv2/calib3d.hpp>
#include "motion.h"

nanobot::vision::Motion::Motion(
    cv::Mat camera_matrix,
    cv::Mat dist_coeffs) : Motion(camera_matrix, dist_coeffs, 0.6f)
{
}

nanobot::vision::Motion::Motion(
    cv::Mat camera_matrix,
    cv::Mat dist_coeffs,
    float match_threshold) : _camera_matrix(camera_matrix), _dist_coeffs(dist_coeffs), _match_threshold(match_threshold)
{
  _camera_matrix_inv = camera_matrix.inv();
  _detector = cv::BRISK::create();
  _matcher = cv::FlannBasedMatcher::create();
}

bool nanobot::vision::Motion::compute(
    const cv::cuda::GpuMat &d_frame_gray,
    const cv::Mat &depth_map,
    cv::Mat &rmat,
    cv::Mat &tvec)
{
  std::vector<cv::KeyPoint> keypoints;
  cv::Mat descriptors;

  _detector->detectAndCompute(cv::Mat(d_frame_gray), cv::Mat(), keypoints, descriptors);

  if (descriptors.type() != CV_32F)
  {
    descriptors.convertTo(descriptors, CV_32F);
  }

  bool matched = false;

  // Performing matching is possible.
  if (!_previous_descriptors.empty() && !_previous_keypoints.empty())
  {
    matched = match(descriptors);
  }

  bool solved = false;

  if (matched)
  {
    solved =
        depth_map.empty() ? solveE(keypoints, rmat, tvec)
                          : solvePnP(keypoints, depth_map, rmat, tvec);
  }

  // Forward keypoints and descriptors.
  d_frame_gray.copyTo(_d_previous_frame_gray);
  descriptors.copyTo(_previous_descriptors);
  _previous_keypoints = keypoints;

  return solved;
}

bool nanobot::vision::Motion::match(const cv::Mat &descriptors)
{
  // match descriptors
  std::vector<std::vector<cv::DMatch>> all_matches;
  if (_previous_descriptors.rows < 2 || descriptors.rows < 2)
  {
    return false;
  }
  _matcher->knnMatch(_previous_descriptors, descriptors, all_matches, 2);

  // filter matches
  std::vector<cv::DMatch> matches;
  for (std::vector<cv::DMatch> all_match : all_matches)
  {
    if (all_match.size() != 2)
    {
      continue;
    }
    if (all_match[0].distance < _match_threshold * all_match[1].distance)
    {
      matches.push_back(all_match[0]);
    }
  }
  _matches = matches;
  return true;
}

bool nanobot::vision::Motion::solveE(
    const std::vector<cv::KeyPoint> &keypoints,
    cv::Mat &rmat,
    cv::Mat &tvec)
{
  if (_matches.empty())
  {
    return false;
  }

  std::vector<cv::Point2d> previous_image_points;
  std::vector<cv::Point2d> image_points;

  for (cv::DMatch match : _matches)
  {
    cv::Point2f previous_point = _previous_keypoints.at(match.queryIdx).pt;
    cv::Point2f point = keypoints.at(match.trainIdx).pt;
    previous_image_points.push_back(previous_point);
    image_points.push_back(point);
  }

  if (previous_image_points.size() < 4)
  {
    return false;
  }

  // calculate motion.
  std::vector<uchar> mask;
  cv::Mat e = cv::findEssentialMat(previous_image_points, image_points, _camera_matrix, cv::RANSAC, 0.999, 1.0, mask);
  if (e.rows != 3 || e.cols != 3)
  {
    return false;
  }
  cv::recoverPose(e, previous_image_points, image_points, _camera_matrix, rmat, tvec, mask);

  // record filtered image points
  std::vector<cv::Point2d> previous_image_points_filtered;
  std::vector<cv::Point2d> image_points_filtered;
  for (int i = 0; i < mask.size(); i++)
  {
    if (mask[i] == 0)
    {
      continue;
    }
    previous_image_points_filtered.push_back(previous_image_points[i]);
    image_points_filtered.push_back(image_points[i]);
  }
  _previous_image_points = previous_image_points_filtered;
  _image_points = image_points_filtered;

  return true;
}

bool nanobot::vision::Motion::solvePnP(
    const std::vector<cv::KeyPoint> &keypoints,
    const cv::Mat &depth_map,
    cv::Mat &rmat,
    cv::Mat &tvec)
{
  if (_matches.empty() || depth_map.empty())
  {
    return false;
  }

  std::vector<cv::Point3d> object_points;
  std::vector<cv::Point2d> previous_image_points;
  std::vector<cv::Point2d> image_points;

  for (cv::DMatch match : _matches)
  {
    cv::Point2f previous_point = _previous_keypoints.at(match.queryIdx).pt;
    cv::Point2f point = keypoints.at(match.trainIdx).pt;
    float z = depth_map.at<float>((int)round(previous_point.y), (int)round(previous_point.x));
    if (isinf(z))
    {
      continue;
    }
    cv::Vec3d object_vec((double)previous_point.x * z, (double)previous_point.y * z, (double)z);
    cv::Mat object_pt = _camera_matrix_inv * cv::Mat(object_vec);
    cv::Point3d object_point(object_pt);
    object_points.push_back(object_point);
    previous_image_points.push_back(previous_point);
    image_points.push_back(point);
  }

  if (object_points.size() <= 6)
  {
    return false;
  }

  // calculate motion.
  std::vector<int> inliners;
  cv::Mat rvec;
  try
  {
    cv::solvePnPRansac(
        object_points, image_points,
        _camera_matrix, _dist_coeffs,
        rvec, tvec,
        false, 100, 8.0, 0.99,
        inliners);
  }
  catch (const cv::Exception &ex)
  {
    std::cerr  << "Solve PnP error: insufficient inliners \n" << ex.what() << std::endl;
    return false;
  }
  cv::Rodrigues(rvec, rmat);

  // record filtered image points
  std::vector<cv::Point2d> previous_image_points_filtered;
  std::vector<cv::Point2d> image_points_filtered;
  for (int inliner : inliners)
  {
    previous_image_points_filtered.push_back(previous_image_points[inliner]);
    image_points_filtered.push_back(image_points[inliner]);
  }
  _previous_image_points = previous_image_points_filtered;
  _image_points = image_points_filtered;

  return true;
}

char nanobot::vision::Motion::displayMotion(const cv::Mat &frame, std::string name, int delay) const
{
  std::vector<cv::Point2d> previous_image_points = _previous_image_points;
  std::vector<cv::Point2d> image_points = _image_points;

  if (previous_image_points.empty() || image_points.empty() || previous_image_points.size() != image_points.size())
  {
    return (char)0;
  }

  cv::Mat frame_motion;
  frame.copyTo(frame_motion);

  cv::Scalar red(0, 0, 255);
  cv::Scalar green(0, 255, 0);

  for (int i = 0; i < image_points.size(); i++)
  {
    cv::Point previous_point = previous_image_points[i];
    cv::Point point = image_points[i];
    cv::circle(frame_motion, previous_point, 5, green);
    cv::arrowedLine(frame_motion, previous_point, point, red);
    cv::circle(frame_motion, point, 5, red);
  }

  cv::imshow(name, frame_motion);
  return cv::waitKey(delay);
}