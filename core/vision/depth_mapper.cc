#include <iostream>
#include <stdlib.h>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudawarping.hpp>
#include "depth_mapper.h"

nanobot::vision::DepthMapper::DepthMapper(float focal_length, float baseline)
    : focal_length(focal_length), baseline(baseline), num_disparities(96)
{
  init();
}

nanobot::vision::DepthMapper::DepthMapper(float focal_length, float baseline, int num_disparities)
    : focal_length(focal_length), baseline(baseline), num_disparities(num_disparities)
{
  init();
}

void nanobot::vision::DepthMapper::init() {
  int block_size = 21;
  int texture_threshold = 2;

  matcher = cv::cuda::createStereoBM(num_disparities, block_size);
  matcher->setTextureThreshold(texture_threshold);
}

void nanobot::vision::DepthMapper::map(
    const cv::cuda::GpuMat &d_frame_left_gray,
    const cv::cuda::GpuMat &d_frame_right_gray,
    cv::cuda::GpuMat &d_disparity,
    cv::cuda::GpuMat &d_depth_map,
    cv::cuda::Stream &stream) const
{
  // Construct disparity map.
  matcher->compute(d_frame_left_gray, d_frame_right_gray, d_disparity, stream);

  // Convert to float.
  cv::cuda::GpuMat d_disparity_f;
  d_disparity.convertTo(d_disparity_f, CV_32F, stream);

  // Calculate depth = f * b / disp.
  cv::cuda::GpuMat d_disparity_inv;
  cv::cuda::pow(d_disparity_f, -1.0, d_disparity_inv, stream);
  d_disparity_inv.convertTo(d_depth_map, CV_32F, focal_length * baseline, stream);
}