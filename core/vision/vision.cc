#include <iostream>
#include <stdlib.h>
#include <fmt/core.h>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/photo/cuda.hpp>
#include "calibration.h"
#include "vision.h"
#include "../common/f_timer.h"

#define CALIBRATION_FILE "camera.dat"
#define BASELINE 0.06f
#define PATTERN_LENGTH 0.02f
#define PATTERN_SIZE_H 11
#define PATTERN_SIZE_V 8

#define ENABLE_DENOISING false
#define ENABLE_DEPTH true

nanobot::vision::Vision::Vision(
    int interval,
    cv::Size image_size,
    MotionCallback motion_fn) : Vision(interval, image_size, motion_fn, CALIBRATION_FILE, 0, 1)
{
}

nanobot::vision::Vision::Vision(
    int interval,
    cv::Size image_size,
    MotionCallback motion_fn,
    std::string calibration_file,
    int left_id,
    int right_id) : _motion_fn(motion_fn), _calibration_file(calibration_file), _image_size(image_size)
{
  // initiallize camera.
  _camera_left = camera(left_id);
  _camera_right = camera(right_id);
  // initiallize timer.
  _timer = std::make_shared<nanobot::common::FTimer>(interval);
}

cv::VideoCapture nanobot::vision::Vision::camera(int id)
{
  std::string config = fmt::format("nvarguscamerasrc sensor-id={} ! video/x-raw(memory:NVMM), width=1280, height=720, format=(string)NV12, framerate=(fraction)60/1 ! nvvidconv flip-method=2 ! video/x-raw, width={}, height={}, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink wait-on-eos=false max-buffers=1 drop=true", id, _image_size.width, _image_size.height);

  return cv::VideoCapture(config, cv::CAP_GSTREAMER);
}

char nanobot::vision::Vision::displayDisparity(std::string name, int delay) const
{
  if (_d_disparity.empty())
  {
    return (char)0;
  }

  cv::cuda::GpuMat d_disparity_color;
  cv::cuda::drawColorDisp(_d_disparity, d_disparity_color, 96);

  cv::Mat disparity_color = cv::Mat(d_disparity_color);
  cv::imshow(name, disparity_color);
  return cv::waitKey(delay);
}

char nanobot::vision::Vision::displayMotion(std::string name, int delay) const
{
  if (_d_frame_left.empty() || this->_motion == nullptr)
  {
    return (char)0;
  }

  cv::Mat pose;
  cv::Rodrigues(_pose, pose);
  std::cout << "Pose: " << std::endl
            << _tran_mat * pose << std::endl;
  std::cout << "Position: " << std::endl
            << _tran_mat * _position << std::endl;

  cv::Mat frame_left = cv::Mat(_d_frame_left);
  return this->_motion->displayMotion(frame_left, name, delay);
}

void nanobot::vision::Vision::start()
{
  if (!_camera_left.isOpened())
  {
    std::cerr << "Camera (Left) is not opened" << std::endl;
  }

  if (!_camera_right.isOpened())
  {
    std::cerr << "Camera (Right) is not opened" << std::endl;
  }

  if (!_calibrated)
  {
    loadCalibration();
  }

  std::cout << "Load Camera Calibration:" << std::endl
            << "camera_matrix_left: " << _camera_matrix_left << std::endl
            << "camera_matrix_right: " << _camera_matrix_right << std::endl
            << "dist_coeffs_left: " << _dist_coeffs_left << std::endl
            << "dist_coeffs_right: " << _dist_coeffs_right << std::endl
            << "rectification: " << _rectification << std::endl;

  _depth_mapper = std::make_shared<nanobot::vision::DepthMapper>(_camera_matrix_left.at<double>(0, 0), BASELINE);
  _motion = std::make_shared<nanobot::vision::Motion>(_camera_matrix_left, _dist_coeffs_left);

  _timer->start([this]() {
    this->_camera_left.grab();
    this->_camera_right.grab();

    cv::Mat frame_left;
    this->_camera_left.retrieve(frame_left);

    cv::Mat frame_right;
    this->_camera_right.retrieve(frame_right);

    cv::cuda::Stream stream_left;
    cv::cuda::Stream stream_right;

    cv::cuda::GpuMat d_frame_left;
    cv::cuda::GpuMat d_frame_right;
    _d_frame_left.upload(frame_left, stream_left);
    _d_frame_right.upload(frame_right, stream_right);

    // Convert frames to grayscale.
    cv::cuda::GpuMat d_frame_left_gray;
    cv::cuda::GpuMat d_frame_right_gray;
    cv::cuda::cvtColor(_d_frame_left, d_frame_left_gray, cv::COLOR_BGR2GRAY, 0, stream_left);
    cv::cuda::cvtColor(_d_frame_right, d_frame_right_gray, cv::COLOR_BGR2GRAY, 0, stream_right);

    if (ENABLE_DENOISING)
    {
      cv::cuda::fastNlMeansDenoising(d_frame_left_gray, d_frame_left_gray, 3.0f, 3, 7, stream_left);
      cv::cuda::fastNlMeansDenoising(d_frame_right_gray, d_frame_right_gray, 3.0f, 3, 7, stream_right);
    }

    stream_left.waitForCompletion();
    stream_right.waitForCompletion();

    cv::Mat depth_map;
    cv::cuda::Stream stream_depth;
    if (ENABLE_DEPTH)
    {
      this->_d_depth_map.download(depth_map);
      this->_depth_mapper->map(
          d_frame_left_gray, d_frame_right_gray,
          this->_d_disparity, this->_d_depth_map,
          stream_depth);
      stream_depth.waitForCompletion();
    }

    cv::Mat rmat, tvec;
    bool succeed = this->_motion->compute(d_frame_left_gray, depth_map, rmat, tvec);

    if (ENABLE_DEPTH)
    {
      stream_depth.waitForCompletion();
    }

    if (!succeed)
    {
      return;
    }

    this->_motion_fn(rmat, tvec);
  });
}

void nanobot::vision::Vision::stop()
{
  _timer->stop();

  _camera_left.release();
  _camera_right.release();
}

double nanobot::vision::Vision::calibrate()
{
  if (!_camera_left.isOpened())
  {
    std::cerr << "Camera (Left) is not opened" << std::endl;
  }

  if (!_camera_right.isOpened())
  {
    std::cerr << "Camera (Right) is not opened" << std::endl;
  }

  // Calibrate cameras.
  nanobot::vision::Calibration calibration(
      _image_size,
      cv::Size(PATTERN_SIZE_H, PATTERN_SIZE_V),
      PATTERN_LENGTH,
      BASELINE);
  calibration.display(true);
  double error = calibration.stereoCalibrate(
      _camera_left, _camera_right,
      _camera_matrix_left, _dist_coeffs_left,
      _camera_matrix_right, _dist_coeffs_right,
      _rectification);

  // Write camera calibration.
  cv::FileStorage file(_calibration_file, cv::FileStorage::WRITE);
  file << "camera_matrix_left" << _camera_matrix_left
       << "camera_matrix_right" << _camera_matrix_right
       << "dist_coeffs_left" << _dist_coeffs_left
       << "dist_coeffs_right" << _dist_coeffs_right
       << "rectification" << _rectification;
  file.release();

  _calibrated = true;

  return error;
}

void nanobot::vision::Vision::loadCalibration()
{
  cv::FileStorage file(_calibration_file, cv::FileStorage::READ);

  if (!file.isOpened())
  {
    std::cerr << "Failed to open camera calibration file" << std::endl;
    return;
  }

  file["camera_matrix_left"] >> _camera_matrix_left;
  file["camera_matrix_right"] >> _camera_matrix_right;
  file["dist_coeffs_left"] >> _dist_coeffs_left;
  file["dist_coeffs_right"] >> _dist_coeffs_right;
  file["rectification"] >> _rectification;

  file.release();

  _calibrated = true;
}