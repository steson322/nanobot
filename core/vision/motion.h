#ifndef VISION_MOTION
#define VISION_MOTION

#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/core/types.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/cudafeatures2d.hpp>

namespace nanobot::vision
{
  class Motion
  {
  private:
    cv::Mat _camera_matrix;
    cv::Mat _camera_matrix_inv;
    cv::Mat _dist_coeffs;
    float _match_threshold;
    cv::Ptr<cv::Feature2D> _detector;
    cv::Ptr<cv::FlannBasedMatcher> _matcher;
    cv::cuda::GpuMat _d_previous_frame_gray;
    std::vector<cv::KeyPoint> _previous_keypoints;
    cv::Mat _previous_descriptors;
    std::vector<cv::DMatch> _matches;
    std::vector<cv::Point2d> _previous_image_points;
    std::vector<cv::Point2d> _image_points;
    bool match(const cv::Mat &descriptors);
    bool solveE(
        const std::vector<cv::KeyPoint> &keypoints,
        cv::Mat &rmat,
        cv::Mat &tvec);
    bool solvePnP(
        const std::vector<cv::KeyPoint> &keypoints,
        const cv::Mat &depth_map,
        cv::Mat &rmat,
        cv::Mat &tvec);

  public:
    Motion(cv::Mat camera_matrix, cv::Mat dist_coeffs);
    Motion(cv::Mat camera_matrix, cv::Mat dist_coeffs, float match_threshold);
    bool compute(
        const cv::cuda::GpuMat &d_frame_gray,
        const cv::Mat &depth_map,
        cv::Mat &rmat,
        cv::Mat &tvec);
    char displayMotion(const cv::Mat &frame, std::string name, int delay) const;
  };
}; // namespace nanobot::vision

#endif /* !VISION_MOTION */