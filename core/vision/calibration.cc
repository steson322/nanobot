#include <iostream>
#include <stdlib.h>
#include <opencv2/photo/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>
#include "calibration.h"

nanobot::vision::Calibration::Calibration(
    cv::Size image_size,
    cv::Size pattern_size,
    float pattern_length,
    float baseline) : image_size(image_size),
                      pattern_size(pattern_size),
                      sample_counts(10),
                      baseline(baseline),
                      display_image(false)
{
  for (int i = 0; i < pattern_size.height; i++)
  {
    for (int j = 0; j < pattern_size.width; j++)
    {
      object_corners.push_back(cv::Point3f(pattern_length * (float)j, pattern_length * (float)i, 0.0f));
    }
  }
}

void nanobot::vision::Calibration::display(bool display_image)
{
  this->display_image = display_image;
}

bool nanobot::vision::Calibration::capture(
    cv::Mat &frame,
    std::vector<std::vector<cv::Point3f>> &object_points,
    std::vector<std::vector<cv::Point2f>> &image_points) const
{
  // Convert frame to gray scale.
  cv::cuda::GpuMat d_frame;
  d_frame.upload(frame);
  cv::cuda::GpuMat d_frame_gray;
  cv::cuda::cvtColor(d_frame, d_frame_gray, cv::COLOR_BGR2GRAY);
  cv::cuda::fastNlMeansDenoising(d_frame_gray, d_frame_gray, 10.0f);
  cv::Mat frame_gray = cv::Mat(d_frame_gray);

  // Identify image corners.
  std::vector<cv::Point2f> image_corners;
  bool pattern_found = cv::findChessboardCorners(frame_gray, pattern_size, image_corners, cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE + cv::CALIB_CB_FAST_CHECK);

  if (pattern_found)
  {
    // Refine image corners.
    cv::TermCriteria corner_term_criteria = cv::TermCriteria(
        cv::TermCriteria::EPS + cv::TermCriteria::MAX_ITER, 30, 0.1);
    cv::cornerSubPix(frame_gray, image_corners, cv::Size(11, 11), cv::Size(-1, -1), corner_term_criteria);

    // Add point.
    object_points.push_back(object_corners);
    image_points.push_back(image_corners);
  }

  if (display_image)
  {
    cv::drawChessboardCorners(frame, pattern_size, cv::Mat(image_corners), pattern_found);
  }

  return true;
}

double nanobot::vision::Calibration::calibrate(
    cv::VideoCapture &camera,
    cv::Mat &camera_matrix,
    cv::Mat &dist_coeffs) const
{
  std::vector<std::vector<cv::Point3f>> object_points;
  std::vector<std::vector<cv::Point2f>> image_points;

  // Capture frames.
  while (object_points.size() < sample_counts)
  {
    cv::Mat frame;
    camera.read(frame);
    capture(frame, object_points, image_points);

    // Output state.
    std::cout << "Sampling for calibration: " << object_points.size() << std::endl;
    if (display_image)
    {
      cv::imshow("Calibration", frame);
    }

    // Listen for termination.
    char key = (char)cv::waitKey(50);
    if (key == 27 || key == 113)
    {
      return INFINITY;
    }
  }

  // Calibrate camera.
  std::vector<cv::Mat> rvecs, tvecs;
  return cv::calibrateCamera(object_points, image_points, image_size, camera_matrix, dist_coeffs, rvecs, tvecs,
                             cv::CALIB_FIX_PRINCIPAL_POINT + cv::CALIB_FIX_ASPECT_RATIO);
}

double nanobot::vision::Calibration::stereoCalibrate(
    cv::VideoCapture &camera_left,
    cv::VideoCapture &camera_right,
    cv::Mat &camera_matrix_left,
    cv::Mat &dist_coeffs_left,
    cv::Mat &camera_matrix_right,
    cv::Mat &dist_coeffs_right,
    cv::Mat &rectification) const
{
  std::vector<std::vector<cv::Point3f>> object_points;
  std::vector<std::vector<cv::Point2f>> image_points_left;

  // Capture Left Camera.
  while (image_points_left.size() < sample_counts)
  {
    cv::Mat frame;
    camera_left.read(frame);
    capture(frame, object_points, image_points_left);

    // Output state.
    std::cout << "Sampling (Left) for calibration: " << image_points_left.size() << std::endl;
    if (display_image)
    {
      cv::imshow("Calibration", frame);
    }

    // Listen for termination.
    char key = (char)cv::waitKey(50);
    if (key == 27 || key == 113)
    {
      return INFINITY;
    }
  }

  std::vector<std::vector<cv::Point3f>> object_points_dummy;
  std::vector<std::vector<cv::Point2f>> image_points_right;

  // Capture Right Camera.
  while (image_points_right.size() < sample_counts)
  {
    cv::Mat frame;
    camera_right.read(frame);
    capture(frame, object_points_dummy, image_points_right);

    // Output state.
    std::cout << "Sampling (Right) for calibration: " << image_points_right.size() << std::endl;
    if (display_image)
    {
      cv::imshow("Calibration", frame);
    }

    // Listen for termination.
    char key = (char)cv::waitKey(50);
    if (key == 27 || key == 113)
    {
      return INFINITY;
    }
  }

  // Calibrate camera.
  cv::Mat rvecs = cv::Mat::eye(3, 3, CV_32F);
  cv::Vec3d tvecs(baseline, 0, 0);
  cv::Mat e_mat, f_mat;
  double error = cv::stereoCalibrate(
      object_points,
      image_points_left, image_points_right,
      camera_matrix_left, dist_coeffs_left,
      camera_matrix_right, dist_coeffs_right,
      image_size,
      rvecs, tvecs,
      e_mat, f_mat,
      cv::CALIB_FIX_PRINCIPAL_POINT +
      cv::CALIB_ZERO_TANGENT_DIST +
      cv::CALIB_SAME_FOCAL_LENGTH);


  cv::Mat r_left, r_right;
  cv::Mat p_left, p_right;
  cv::stereoRectify(
      camera_matrix_left, dist_coeffs_left,
      camera_matrix_right, dist_coeffs_right,
      image_size,
      rvecs, tvecs,
      r_left, r_right,
      p_left, p_right,
      rectification
  );

  return error;
}