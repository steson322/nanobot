#include <iostream>
#include <chrono>
#include <math.h>
#include <Eigen/Geometry>
#include "sensor.h"
#include "../common/f_timer.h"

#define EARTH_G 9.80665
#define RAD_PER_DEG (M_PI / 180.0)
#define ACCEL_ERROR 0.01
#define GYRO_ERROR 0.01
#define MAG_ERROR 0.01

#define ACCEL_POSE_EST_LIMIT 0.1

nanobot::sensor::Sensor::Sensor(int interval)
{
  // initiallize timer.
  _timer = std::make_shared<nanobot::common::FTimer>(interval);
  // initiallize IMU.
  _imu = std::make_shared<IMU>();
  // initialize static matrices.
  double accel_std = 2.0 * ACCEL_ERROR;
  double gyro_std = RAD_PER_DEG * 1000.0 * GYRO_ERROR;
  double mag_std = MAG_ERROR;
  Eigen::VectorXd q_vec(6);
  q_vec(0) = q_vec(1) = q_vec(2) = accel_std * accel_std * EARTH_G * EARTH_G;
  q_vec(3) = q_vec(4) = q_vec(5) = gyro_std * gyro_std;
  _q_cov = q_vec.asDiagonal();
  _l_jac.bottomRows(6) = Eigen::MatrixXd::Identity(6, 6);

  _h_jac_vo.topLeftCorner(3, 3) = Eigen::MatrixXd::Identity(3, 3);
  _h_jac_vo.bottomRightCorner(3, 3) = Eigen::MatrixXd::Identity(3, 3);

  Eigen::VectorXd var_pose_vec(3);
  var_pose_vec(0) = var_pose_vec(1) = accel_std * accel_std;
  var_pose_vec(2) = mag_std * mag_std;
  _var_pose = var_pose_vec.asDiagonal();
  _h_jac_pose.rightCols(3) = Eigen::MatrixXd::Identity(3, 3);

  // Translation quaternion between imu and vehicle.
  _imu_to_vehicle = Eigen::AngleAxisd(-M_PI / 8, Eigen::Vector3d::UnitY());
}

void nanobot::sensor::Sensor::start()
{
  if (!_imu->init())
  {
    std::cerr << "Unable to initialize IMU" << std::endl;
    return;
  }

  _timer->start([this]() {
    this->process();
  });
}

void nanobot::sensor::Sensor::process()
{
  _critical.lock();

  // Read plain accel and gyro relative to imu.
  Eigen::Vector3d accel, gyro, mag;
  double delta_t = _imu->read(accel, gyro, mag);

  // Correct accel and mag to vehicle frame.
  accel = _imu_to_vehicle.toRotationMatrix() * accel;
  mag = _imu_to_vehicle.toRotationMatrix() * mag;

  Eigen::Vector3d pose_mea;
  bool pose_mea_valid = measurePose(accel, mag, pose_mea);

  // If not stablized, wait for at least one measurement.
  if (!_is_stablized) {
    if (pose_mea_valid)
    {
      _pose = Eigen::AngleAxisd(pose_mea(0), Eigen::Vector3d::UnitX()) *
              Eigen::AngleAxisd(pose_mea(1), Eigen::Vector3d::UnitY()) *
              Eigen::AngleAxisd(pose_mea(2), Eigen::Vector3d::UnitZ());
      _is_stablized = true;
    }
    _critical.unlock();
    return;
  }

  // Correct acceleration by pose.
  accel = _pose.toRotationMatrix() * (accel * EARTH_G);

  // convert accel and gyro to timed standard format
  Eigen::Vector3d g(0, 0, -EARTH_G);
  Eigen::Vector3d accel_net = accel - g;

  Eigen::Quaterniond gyro_quat;
  gyro_quat = Eigen::AngleAxisd(gyro(0) * delta_t, Eigen::Vector3d::UnitX()) *
              Eigen::AngleAxisd(gyro(1) * delta_t, Eigen::Vector3d::UnitY()) *
              Eigen::AngleAxisd(gyro(2) * delta_t, Eigen::Vector3d::UnitZ());

  _position = _position + delta_t * _velocity + 0.5 * delta_t * delta_t * accel_net;
  _velocity = _velocity + delta_t * accel_net;
  _pose = _pose * (_imu_to_vehicle * gyro_quat * _imu_to_vehicle.inverse());

  // Jacobian for process.
  Eigen::MatrixXd F = Eigen::MatrixXd::Identity(9, 9);
  F.block<3, 3>(0, 3) = Eigen::MatrixXd::Identity(3, 3) * delta_t;
  F.block<3, 3>(3, 6) = toSkewSymmetric(accel) * -delta_t;

  // Covariance for process.
  Eigen::MatrixXd Q = _q_cov * delta_t * delta_t;

  // Update estimate covariance.
  _p_cov = F * _p_cov * F.transpose() + _l_jac * Q * _l_jac.transpose();

  if (pose_mea_valid)
  {
    estimateByPose(pose_mea);
  }

  _critical.unlock();
}

bool nanobot::sensor::Sensor::measurePose(
    const Eigen::Vector3d &accel,
    const Eigen::Vector3d &mag,
    Eigen::Vector3d &pose) const
{
  double accel_mg = accel.dot(accel);

  // if magnitude of the accel is too much from gravity, don't use it.
  if (std::abs(accel_mg - 1.0) > ACCEL_POSE_EST_LIMIT)
  {
    return false;
  }

  Eigen::Vector3d accel_unit = accel / accel_mg;
  Eigen::Vector3d accel_g(0, 0, -1.0);

  Eigen::Quaterniond r_quat = Eigen::Quaterniond::FromTwoVectors(accel_unit, accel_g);
  
  Eigen::Vector3d r_euler = toEuler(r_quat);

  Eigen::Quaterniond r_xy_quat;
  r_xy_quat = Eigen::AngleAxisd(r_euler(0), Eigen::Vector3d::UnitX()) *
              Eigen::AngleAxisd(r_euler(1), Eigen::Vector3d::UnitY()) *
              Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitZ());

  Eigen::Vector3d mag_adj = r_xy_quat.toRotationMatrix() * mag;

  double yaw = -std::atan2(mag_adj(1), mag_adj(0));

  pose = Eigen::Vector3d(r_euler(0), r_euler(1), yaw);

  return true;
}

void nanobot::sensor::Sensor::estimateByPose(const Eigen::Vector3d &pose)
{
  estimate(pose, _h_jac_pose, _var_pose);
}

void nanobot::sensor::Sensor::estimateByVision(
    const Eigen::Vector3d &position,
    const Eigen::Vector3d &velocity,
    const Eigen::Vector3d &pose,
    const Eigen::MatrixXd &var)
{
  _critical.lock();

  Eigen::VectorXd measure(6);
  measure << position, pose;

  estimate(measure, _h_jac_vo, var);

  _critical.unlock();
}

void nanobot::sensor::Sensor::estimate(
  const Eigen::VectorXd &measure, 
  const Eigen::MatrixXd &h_jac, 
  const Eigen::MatrixXd &var)
{
  // Calculate kalman gain.
  Eigen::MatrixXd inner = h_jac * _p_cov * h_jac.transpose() + var;
  Eigen::MatrixXd k = _p_cov * h_jac.transpose() * inner.inverse();

  // Compute error.
  Eigen::VectorXd x(9);
  x << _position, _velocity, toEuler(_pose);

  Eigen::VectorXd e = measure - h_jac * x;

  // Compute predicted state.
  Eigen::VectorXd error = k * e;
  Eigen::Vector3d position_error = error.topRows(3);
  Eigen::Vector3d velocity_error = error.block(3, 0, 3, 1);
  Eigen::Vector3d pose_error = error.bottomRows(3);

  Eigen::Quaterniond pose_error_quat;
  pose_error_quat = Eigen::AngleAxisd(pose_error(0), Eigen::Vector3d::UnitX()) *
                    Eigen::AngleAxisd(pose_error(1), Eigen::Vector3d::UnitY()) *
                    Eigen::AngleAxisd(pose_error(2), Eigen::Vector3d::UnitZ());

  _position = _position + position_error;
  _velocity = _velocity + velocity_error;
  _pose = pose_error_quat * _pose;

  // Compute predicted covariance.
  Eigen::MatrixXd i = Eigen::MatrixXd::Identity(9, 9);
  _p_cov = (_i9 - k * h_jac) * _p_cov;
}

Eigen::Matrix3d nanobot::sensor::Sensor::toSkewSymmetric(Eigen::Vector3d vector) const
{
  Eigen::Matrix3d m;
  m << 0, -vector[2], vector[1],
      vector[2], 0, -vector[0],
      -vector[1], vector[0], 0;
  return m;
}

Eigen::Vector3d nanobot::sensor::Sensor::toEuler(Eigen::Quaterniond quat) const
{
  double w = quat.w();
  double x = quat.x();
  double y = quat.y();
  double z = quat.z();

  double roll = atan2(2.0 * (w * x + y * z), 1.0 - 2.0 * (x * x + y * z));
  double pitch = asin(2.0 * (w * y - z * x));
  double yaw = atan2(2.0 * (w * z + x * y), 1.0 - 2.0 * (y * y + z * z));

  return Eigen::Vector3d(roll, pitch, yaw);
}

void nanobot::sensor::Sensor::stop()
{
  _timer->stop();
}

void nanobot::sensor::Sensor::getState(
    Eigen::Vector3d &position,
    Eigen::Vector3d &velocity,
    Eigen::Matrix3d &pose) const
{
  position = _position;
  velocity = _velocity;
  pose = _pose.toRotationMatrix();
}

void nanobot::sensor::Sensor::printState() const
{
  Eigen::Vector3d pose = toEuler(_pose);

  std::cout << std::endl
            << "Position(XYZ): " << std::endl
            << _position << std::endl
            << "Velocity(XYZ): " << std::endl
            << _velocity << std::endl
            << "Pose(RPY): " << std::endl
            << pose * 180.0 / M_PI << std::endl;
}