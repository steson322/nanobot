#ifndef SENSOR_IMU
#define SENSOR_IMU

#include <stdlib.h>
#include <thread>
#include <memory>
#include <eigen3/Eigen/Dense>
#include "../common/i2c.h"

namespace nanobot::sensor
{
  class IMU
  {
  private:
    std::string _imu_calibration_file;
    std::string _mag_calibration_file;
    std::shared_ptr<common::I2C> _i2c;
    Eigen::Vector3d _accel_offset;
    Eigen::Vector3d _gyro_offset;
    Eigen::Vector3d _mag_offset;
    Eigen::Vector3d _mag_scale;
    std::chrono::high_resolution_clock::time_point _previous_time;
    void readRaw(Eigen::Vector3d &accel, Eigen::Vector3d &gyro, Eigen::Vector3d &mag);
    void readAccel(int16_t &x, int16_t &y, int16_t &z);
    void readGyro(int16_t &x, int16_t &y, int16_t &z);
    void readMag(int16_t &x, int16_t &y, int16_t &z);
    void readSecondary(uint8_t slv_addr, uint8_t reg_addr, uint8_t length, uint8_t *ret_val);
    void writeSecondary(uint8_t slv_addr, uint8_t reg_addr, uint8_t val);
    void loadCalibration();

  public:
    IMU();
    IMU(std::string path, std::string imu_calibration_file, std::string mag_calibration_file);
    bool init();
    void calibrate();
    void calibrateMag();
    double read(Eigen::Vector3d &accel, Eigen::Vector3d &gyro, Eigen::Vector3d &mag);
  };
}; // namespace nanobot::sensor

#endif /* !SENSOR_IMU */