#include <iostream>
#include <fstream>
#include <chrono>
#include <math.h>
#include <Eigen/Geometry>
#include "imu.h"

#define I2C_PATH "/dev/i2c-0"

// Device Address.
#define I2C_ADD_ICM20948 0x68
#define I2C_ADD_ICM20948_AK09916 0x0C
#define I2C_ADD_ICM20948_AK09916_READ 0x80
#define I2C_ADD_ICM20948_AK09916_WRITE 0x00

// Register Bank Selection.
#define REG_ADD_REG_BANK_SEL 0x7F

#define REG_VAL_REG_BANK_0 0x00
#define REG_VAL_REG_BANK_1 0x10
#define REG_VAL_REG_BANK_2 0x20
#define REG_VAL_REG_BANK_3 0x30

// Bank 0 - Common.
#define REG_ADD_WIA 0x00
#define REG_VAL_WIA 0xEA

#define REG_ADD_USER_CTRL 0x03
#define REG_VAL_BIT_DMP_EN 0x80
#define REG_VAL_BIT_FIFO_EN 0x40
#define REG_VAL_BIT_I2C_MST_EN 0x20
#define REG_VAL_BIT_I2C_IF_DIS 0x10
#define REG_VAL_BIT_DMP_RST 0x08
#define REG_VAL_BIT_DIAMOND_DMP_RST 0x04

#define REG_ADD_PWR_MIGMT_1 0x06
#define REG_VAL_ALL_RGE_RESET 0x80
#define REG_VAL_RUN_MODE 0x01 //Non low-power mode

#define REG_ADD_LP_CONFIG 0x05
#define REG_ADD_PWR_MGMT_1 0x06
#define REG_ADD_PWR_MGMT_2 0x07
#define REG_ADD_ACCEL_XOUT_H 0x2D
#define REG_ADD_ACCEL_XOUT_L 0x2E
#define REG_ADD_ACCEL_YOUT_H 0x2F
#define REG_ADD_ACCEL_YOUT_L 0x30
#define REG_ADD_ACCEL_ZOUT_H 0x31
#define REG_ADD_ACCEL_ZOUT_L 0x32
#define REG_ADD_GYRO_XOUT_H 0x33
#define REG_ADD_GYRO_XOUT_L 0x34
#define REG_ADD_GYRO_YOUT_H 0x35
#define REG_ADD_GYRO_YOUT_L 0x36
#define REG_ADD_GYRO_ZOUT_H 0x37
#define REG_ADD_GYRO_ZOUT_L 0x38
#define REG_ADD_EXT_SENS_DATA_00 0x3B

// Bank 1 & 2 - Sensor Configs.
#define REG_ADD_GYRO_SMPLRT_DIV 0x00

#define REG_ADD_GYRO_CONFIG_1 0x01
#define REG_VAL_BIT_GYRO_DLPCFG_2 0x10   /* bit[5:3] */
#define REG_VAL_BIT_GYRO_DLPCFG_4 0x20   /* bit[5:3] */
#define REG_VAL_BIT_GYRO_DLPCFG_6 0x30   /* bit[5:3] */
#define REG_VAL_BIT_GYRO_FS_250DPS 0x00  /* bit[2:1] */
#define REG_VAL_BIT_GYRO_FS_500DPS 0x02  /* bit[2:1] */
#define REG_VAL_BIT_GYRO_FS_1000DPS 0x04 /* bit[2:1] */
#define REG_VAL_BIT_GYRO_FS_2000DPS 0x06 /* bit[2:1] */
#define REG_VAL_BIT_GYRO_DLPF 0x01       /* bit[0]   */

#define REG_ADD_ACCEL_SMPLRT_DIV_2 0x11

#define REG_ADD_ACCEL_CONFIG 0x14
#define REG_VAL_BIT_ACCEL_DLPCFG_2 0x10 /* bit[5:3] */
#define REG_VAL_BIT_ACCEL_DLPCFG_4 0x20 /* bit[5:3] */
#define REG_VAL_BIT_ACCEL_DLPCFG_6 0x30 /* bit[5:3] */
#define REG_VAL_BIT_ACCEL_FS_2g 0x00    /* bit[2:1] */
#define REG_VAL_BIT_ACCEL_FS_4g 0x02    /* bit[2:1] */
#define REG_VAL_BIT_ACCEL_FS_8g 0x04    /* bit[2:1] */
#define REG_VAL_BIT_ACCEL_FS_16g 0x06   /* bit[2:1] */
#define REG_VAL_BIT_ACCEL_DLPF 0x01     /* bit[0]   */

// Bank 3 - Slave Configs.
#define REG_ADD_I2C_SLV0_ADDR 0x03
#define REG_ADD_I2C_SLV0_REG 0x04

#define REG_ADD_I2C_SLV0_CTRL 0x05
#define REG_VAL_BIT_SLV0_EN 0x80
#define REG_VAL_BIT_MASK_LEN 0x07

#define REG_ADD_I2C_SLV0_DO 0x06
#define REG_ADD_I2C_SLV1_ADDR 0x07
#define REG_ADD_I2C_SLV1_REG 0x08
#define REG_ADD_I2C_SLV1_CTRL 0x09
#define REG_ADD_I2C_SLV1_DO 0x0A

// Magnetometer Configs.
#define REG_ADD_MAG_WIA1 0x00
#define REG_VAL_MAG_WIA1 0x48

#define REG_ADD_MAG_WIA2 0x01
#define REG_VAL_MAG_WIA2 0x09

#define REG_ADD_MAG_ST2 0x10
#define REG_ADD_MAG_DATA 0x11

#define REG_ADD_MAG_CNTL2 0x31
#define REG_VAL_MAG_MODE_PD 0x00
#define REG_VAL_MAG_MODE_SM 0x01
#define REG_VAL_MAG_MODE_10HZ 0x02
#define REG_VAL_MAG_MODE_20HZ 0x04
#define REG_VAL_MAG_MODE_50HZ 0x05
#define REG_VAL_MAG_MODE_100HZ 0x08
#define REG_VAL_MAG_MODE_ST 0x10

#define MAG_DATA_LEN 6

// IMU Constants
#define IMU_CALIBRATION_FILE "imu.dat"
#define MAG_CALIBRATION_FILE "mag.dat"
#define INT_16_MAX 32768.0
#define RAD_PER_DEG (M_PI / 180.0)
#define ACCEL_COEF (2.0 / INT_16_MAX)
#define GYRO_COEF (RAD_PER_DEG * 1000.0 / INT_16_MAX)

nanobot::sensor::IMU::IMU() : IMU(I2C_PATH, IMU_CALIBRATION_FILE, MAG_CALIBRATION_FILE)
{
}

nanobot::sensor::IMU::IMU(
    std::string i2c_path, std::string imu_calibration_file, std::string mag_calibration_file)
    : _imu_calibration_file(imu_calibration_file), _mag_calibration_file(mag_calibration_file)
{
  _previous_time = std::chrono::high_resolution_clock::time_point::min();
  // initiallize I2C.
  _i2c = std::make_shared<nanobot::common::I2C>(i2c_path);
}

bool nanobot::sensor::IMU::init()
{
  // Initialize I2C and validate default signal.
  if (!_i2c->init(I2C_ADD_ICM20948, REG_ADD_WIA, REG_VAL_WIA))
  {
    std::cerr << "IMU fails initial value check, retrying." << std::endl;
    uint8_t val;
    // Tickle secondary.
    readSecondary(I2C_ADD_ICM20948_AK09916 | I2C_ADD_ICM20948_AK09916_READ, REG_ADD_MAG_WIA1, 1, &val);
    _i2c->read(I2C_ADD_ICM20948, REG_ADD_WIA, val);
    if (val != REG_VAL_WIA)
    {
      std::cerr << "IMU fails initial value check, fatal." << std::endl;
      return false;
    }
  }

  // Reset Power Mode.
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_0);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_PWR_MIGMT_1, REG_VAL_ALL_RGE_RESET);

  std::this_thread::sleep_for(std::chrono::milliseconds(10));

  _i2c->write(I2C_ADD_ICM20948, REG_ADD_PWR_MIGMT_1, REG_VAL_RUN_MODE);

  // Set Sensor Configurations.
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_2);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_GYRO_SMPLRT_DIV, 0x07);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_GYRO_CONFIG_1,
              REG_VAL_BIT_GYRO_DLPCFG_6 | REG_VAL_BIT_GYRO_FS_1000DPS | REG_VAL_BIT_GYRO_DLPF);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_ACCEL_SMPLRT_DIV_2, 0x07);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_ACCEL_CONFIG,
              REG_VAL_BIT_ACCEL_DLPCFG_6 | REG_VAL_BIT_ACCEL_FS_2g | REG_VAL_BIT_ACCEL_DLPF);

  // Set back to bank 0.
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_0);

  std::this_thread::sleep_for(std::chrono::milliseconds(100));

  uint8_t buffer[2];
  readSecondary(I2C_ADD_ICM20948_AK09916 | I2C_ADD_ICM20948_AK09916_READ, REG_ADD_MAG_WIA1, 2, buffer);
  if ((buffer[0] != REG_VAL_MAG_WIA1) || (buffer[1] != REG_VAL_MAG_WIA2))
  {
    std::cerr << "IMU fails magnetometer check." << std::endl;
    return false;
  }

  // Set Mag Configurations.
  writeSecondary(I2C_ADD_ICM20948_AK09916 | I2C_ADD_ICM20948_AK09916_WRITE,
                 REG_ADD_MAG_CNTL2, REG_VAL_MAG_MODE_20HZ);

  loadCalibration();

  return true;
}

void nanobot::sensor::IMU::loadCalibration()
{
  std::ifstream file_imu(_imu_calibration_file);
  double gyro_offset[3];
  double accel_offset[3];
  if (file_imu.is_open())
  {
    file_imu >> gyro_offset[0];
    file_imu >> gyro_offset[1];
    file_imu >> gyro_offset[2];
    file_imu >> accel_offset[0];
    file_imu >> accel_offset[1];
    file_imu >> accel_offset[2];
    file_imu.close();
  }
  _gyro_offset = Eigen::Vector3d(gyro_offset[0], gyro_offset[1], gyro_offset[2]);
  _accel_offset = Eigen::Vector3d(accel_offset[0], accel_offset[1], accel_offset[2]);
  std::cout << "Load IMU Calibration:" << std::endl
            << "Gyro offset: " << _gyro_offset << std::endl
            << "Accel offset: " << _accel_offset << std::endl;

  std::ifstream file_mag(_mag_calibration_file);
  double mag_max[3];
  double mag_min[3];
  if (file_mag.is_open())
  {
    file_mag >> mag_max[0];
    file_mag >> mag_max[1];
    file_mag >> mag_max[2];
    file_mag >> mag_min[0];
    file_mag >> mag_min[1];
    file_mag >> mag_min[2];
    file_mag.close();
  }
  Eigen::Vector3d mag_max_v(mag_max[0], mag_max[1], mag_max[2]);
  Eigen::Vector3d mag_min_v(mag_min[0], mag_min[1], mag_min[2]);
  std::cout << "Load Magnetometer Calibration:" << std::endl
            << "Magnetometer max: " << mag_max_v << std::endl
            << "Magnetometer min: " << mag_min_v << std::endl;
  _mag_offset = (mag_max_v + mag_min_v) / 2;
  _mag_scale = (mag_max_v - mag_min_v) * 2;
}

void nanobot::sensor::IMU::calibrate()
{
  std::cout << "Calibrating IMU: Please keep device as stationary as possible" << std::endl;

  Eigen::Vector3d accel_sum, gyro_sum;
  int total = 100;
  for (int i = 1; i <= total; i++)
  {
    Eigen::Vector3d accel_raw, gyro_raw, mag_raw;
    readRaw(accel_raw, gyro_raw, mag_raw);

    accel_sum = accel_sum + accel_raw;
    gyro_sum = gyro_sum + gyro_raw;

    std::cout << "Gyro avg: " << std::endl
              << gyro_sum / i << std::endl;
              
    if (i % 10 == 1)
    {
      std::cout << "Calibration in-progress: " << i << "/" << total << std::endl;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }

  _gyro_offset = gyro_sum / total;
  _accel_offset = accel_sum / total;

  std::ofstream file(_imu_calibration_file);
  if (file.is_open())
  {
    file << _gyro_offset << std::endl;
    file << _accel_offset << std::endl;
    file.close();
  }

  std::cout << "IMU Calibration is completed." << std::endl;
}

void nanobot::sensor::IMU::calibrateMag()
{
  std::cout << "Calibrating Magnetometer: Please move devide in all directions" << std::endl;

  Eigen::Vector3d mag_max(-(double)INT_16_MAX, -(double)INT_16_MAX, -(double)INT_16_MAX);
  Eigen::Vector3d mag_min((double)INT_16_MAX, (double)INT_16_MAX, (double)INT_16_MAX);
  int total = 100;
  int count = total;
  while (count > 0)
  {
    Eigen::Vector3d accel_raw, gyro_raw, mag_raw;
    readRaw(accel_raw, gyro_raw, mag_raw);
    for (int dim = 0; dim < 3; dim++)
    {
      if (mag_raw(dim) > mag_max(dim))
      {
        mag_max(dim) = mag_raw(dim);
        count = total;
      }
      if (mag_raw(dim) < mag_min(dim))
      {
        mag_min(dim) = mag_raw(dim);
        count = total;
      }
    }
    count--;
    std::cout << "Mag max: " << std::endl
              << mag_max << std::endl;
    std::cout << "Mag min: " << std::endl
              << mag_min << std::endl;
    if (count % 10 == 0)
    {
      std::cout << "Calibration in-progress: " << count << "/" << total << std::endl;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }

  _mag_offset = (mag_max + mag_min) / 2;
  _mag_scale = (mag_max - mag_min) * 2;

  std::ofstream file(_mag_calibration_file);
  if (file.is_open())
  {
    file << mag_max << std::endl;
    file << mag_min << std::endl;
    file.close();
  }

  std::cout << "Magnetometer Calibration is completed." << std::endl;
}

double nanobot::sensor::IMU::read(Eigen::Vector3d &accel, Eigen::Vector3d &gyro, Eigen::Vector3d &mag)
{
  // Read plain accel, gyro, and mag.
  Eigen::Vector3d accel_raw, gyro_raw, mag_raw;
  readRaw(accel_raw, gyro_raw, mag_raw);

  // Convert accel and gyro to global frame.
  accel = ACCEL_COEF * accel_raw;
  gyro = GYRO_COEF * (gyro_raw - _gyro_offset);
  mag = (mag_raw - _mag_offset).cwiseQuotient(_mag_scale);

  // Calculate time gap.
  std::chrono::high_resolution_clock::time_point time = std::chrono::high_resolution_clock::now();

  if (_previous_time == std::chrono::high_resolution_clock::time_point::min())
  {
    _previous_time = time;
    return 0;
  }

  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time - _previous_time);

  _previous_time = time;

  return duration.count() / 1000000.0;
}

void nanobot::sensor::IMU::readRaw(Eigen::Vector3d &accel, Eigen::Vector3d &gyro, Eigen::Vector3d &mag)
{
  int16_t accel_s[3];
  int16_t gyro_s[3];
  int16_t mag_s[3];

  Eigen::Vector3d accel_sum, gyro_sum, mag_sum;

  int N = 4;
  for (int i = 0; i < N; i++)
  {
    // X' = Pitch, Y' = Yaw, Z' = Roll
    readAccel(accel_s[0], accel_s[1], accel_s[2]);
    readGyro(gyro_s[0], gyro_s[1], gyro_s[2]);
    readMag(mag_s[0], mag_s[1], mag_s[2]);

    // Convert to standard vehicle frame.
    // X(long) = Roll = Z', Y(lat) = Pitch = X', Z(dep) = Yaw = Y',
    accel_sum += Eigen::Vector3d(accel_s[2], accel_s[0], accel_s[1]);
    gyro_sum += Eigen::Vector3d(gyro_s[2], gyro_s[0], gyro_s[1]);
    mag_sum += Eigen::Vector3d(mag_s[2], mag_s[0], mag_s[1]);
  }

  accel = accel_sum / N;
  gyro = gyro_sum / N;
  mag = mag_sum / N;
}

void nanobot::sensor::IMU::readAccel(int16_t &x, int16_t &y, int16_t &z)
{
  uint8_t buffer[2];
  _i2c->read(I2C_ADD_ICM20948, REG_ADD_ACCEL_XOUT_L, buffer[0]);
  _i2c->read(I2C_ADD_ICM20948, REG_ADD_ACCEL_XOUT_H, buffer[1]);
  x = (buffer[1] << 8) | buffer[0];

  _i2c->read(I2C_ADD_ICM20948, REG_ADD_ACCEL_YOUT_L, buffer[0]);
  _i2c->read(I2C_ADD_ICM20948, REG_ADD_ACCEL_YOUT_H, buffer[1]);
  y = (buffer[1] << 8) | buffer[0];

  _i2c->read(I2C_ADD_ICM20948, REG_ADD_ACCEL_ZOUT_L, buffer[0]);
  _i2c->read(I2C_ADD_ICM20948, REG_ADD_ACCEL_ZOUT_H, buffer[1]);
  z = (buffer[1] << 8) | buffer[0];
}

void nanobot::sensor::IMU::readGyro(int16_t &x, int16_t &y, int16_t &z)
{
  uint8_t buffer[2];
  _i2c->read(I2C_ADD_ICM20948, REG_ADD_GYRO_XOUT_L, buffer[0]);
  _i2c->read(I2C_ADD_ICM20948, REG_ADD_GYRO_XOUT_H, buffer[1]);
  x = (buffer[1] << 8) | buffer[0];

  _i2c->read(I2C_ADD_ICM20948, REG_ADD_GYRO_YOUT_L, buffer[0]);
  _i2c->read(I2C_ADD_ICM20948, REG_ADD_GYRO_YOUT_H, buffer[1]);
  y = (buffer[1] << 8) | buffer[0];

  _i2c->read(I2C_ADD_ICM20948, REG_ADD_GYRO_ZOUT_L, buffer[0]);
  _i2c->read(I2C_ADD_ICM20948, REG_ADD_GYRO_ZOUT_H, buffer[1]);
  z = (buffer[1] << 8) | buffer[0];
}

void nanobot::sensor::IMU::readMag(int16_t &x, int16_t &y, int16_t &z)
{
  uint8_t buffer[MAG_DATA_LEN];

  uint8_t counter = 20;
  while (counter > 0)
  {
    readSecondary(I2C_ADD_ICM20948_AK09916 | I2C_ADD_ICM20948_AK09916_READ,
                  REG_ADD_MAG_ST2, 1, buffer);

    if ((buffer[0] & 0x01) != 0)
    {
      break;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    counter--;
  }

  if (counter == 0)
  {
    std::cout << "Magnetometer is unstable" << std::endl;
    return;
  }

  readSecondary(I2C_ADD_ICM20948_AK09916 | I2C_ADD_ICM20948_AK09916_READ,
                REG_ADD_MAG_DATA, MAG_DATA_LEN, buffer);

  x = (buffer[1] << 8) | buffer[0];
  y = -((buffer[3] << 8) | buffer[2]);
  z = -((buffer[5] << 8) | buffer[4]);
}

void nanobot::sensor::IMU::readSecondary(uint8_t slv_addr, uint8_t reg_addr, uint8_t length, uint8_t *ret_val)
{
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_3); //swtich bank3
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_I2C_SLV0_ADDR, slv_addr);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_I2C_SLV0_REG, reg_addr);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_I2C_SLV0_CTRL, REG_VAL_BIT_SLV0_EN | length);

  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_0); //swtich bank0

  uint8_t buffer;

  _i2c->read(I2C_ADD_ICM20948, REG_ADD_USER_CTRL, buffer);
  buffer |= REG_VAL_BIT_I2C_MST_EN;
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_USER_CTRL, buffer);
  std::this_thread::sleep_for(std::chrono::milliseconds(5));
  buffer &= ~REG_VAL_BIT_I2C_MST_EN;
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_USER_CTRL, buffer);

  for (uint8_t i = 0; i < length; i++)
  {
    _i2c->read(I2C_ADD_ICM20948, REG_ADD_EXT_SENS_DATA_00 + i, buffer);
    *(ret_val + i) = buffer;
  }
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_3); //swtich bank3

  _i2c->read(I2C_ADD_ICM20948, REG_ADD_I2C_SLV0_CTRL, buffer);
  buffer &= ~((REG_VAL_BIT_I2C_MST_EN) & (REG_VAL_BIT_MASK_LEN));
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_I2C_SLV0_CTRL, buffer);

  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_0); //swtich bank0
}

void nanobot::sensor::IMU::writeSecondary(uint8_t slv_addr, uint8_t reg_addr, uint8_t val)
{
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_3); //swtich bank3
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_I2C_SLV1_ADDR, slv_addr);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_I2C_SLV1_REG, reg_addr);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_I2C_SLV1_DO, val);
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_I2C_SLV1_CTRL, REG_VAL_BIT_SLV0_EN | 1);

  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_0); //swtich bank0

  uint8_t buffer;

  _i2c->read(I2C_ADD_ICM20948, REG_ADD_USER_CTRL, buffer);
  buffer |= REG_VAL_BIT_I2C_MST_EN;
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_USER_CTRL, buffer);
  std::this_thread::sleep_for(std::chrono::milliseconds(5));
  buffer &= ~REG_VAL_BIT_I2C_MST_EN;
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_USER_CTRL, buffer);

  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_3); //swtich bank3

  _i2c->read(I2C_ADD_ICM20948, REG_ADD_I2C_SLV0_CTRL, buffer);
  buffer &= ~((REG_VAL_BIT_I2C_MST_EN) & (REG_VAL_BIT_MASK_LEN));
  _i2c->write(I2C_ADD_ICM20948, REG_ADD_I2C_SLV0_CTRL, buffer);

  _i2c->write(I2C_ADD_ICM20948, REG_ADD_REG_BANK_SEL, REG_VAL_REG_BANK_0); //swtich bank0
}