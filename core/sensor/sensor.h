#ifndef SENSOR_SENSOR
#define SENSOR_SENSOR

#include <memory>
#include <mutex>
#include <eigen3/Eigen/Dense>
#include "../common/f_timer.h"
#include "../common/i2c.h"
#include "imu.h"

namespace nanobot::sensor
{
  class Sensor
  {
  private:
    std::shared_ptr<common::FTimer> _timer;
    std::shared_ptr<IMU> _imu;
    std::mutex _critical; 
    bool _is_stablized = false;
    // States
    Eigen::Vector3d _position;
    Eigen::Vector3d _velocity;
    Eigen::Quaterniond _pose = Eigen::Quaterniond(1, 0, 0, 0);
    Eigen::Quaterniond _imu_to_vehicle = Eigen::Quaterniond(1, 0, 0, 0);
    Eigen::MatrixXd _i9 = Eigen::MatrixXd::Identity(9, 9);
    Eigen::MatrixXd _p_cov = Eigen::MatrixXd(9, 9);
    Eigen::MatrixXd _q_cov = Eigen::MatrixXd(6, 6);
    Eigen::MatrixXd _l_jac = Eigen::MatrixXd(9, 6);
    Eigen::MatrixXd _h_jac_vo = Eigen::MatrixXd(6, 9);
    Eigen::MatrixXd _h_jac_pose = Eigen::MatrixXd(3, 9);
    Eigen::MatrixXd _var_pose = Eigen::MatrixXd(3, 3);
    Eigen::Matrix3d toSkewSymmetric(Eigen::Vector3d vector) const;
    Eigen::Vector3d toEuler(Eigen::Quaterniond quat) const;
    bool measurePose(const Eigen::Vector3d &accel, const Eigen::Vector3d &mag, Eigen::Vector3d &pose) const;
    void estimateByPose(const Eigen::Vector3d &pose);
    void estimate(const Eigen::VectorXd &measure, const Eigen::MatrixXd &h_jac, const Eigen::MatrixXd &var);

  public:
    Sensor(int interval);
    void start();
    void stop();
    void process();
    void estimateByVision(
      const Eigen::Vector3d &position, 
      const Eigen::Vector3d &velocity, 
      const Eigen::Vector3d &pose,
      const Eigen::MatrixXd &var);
    void getState(Eigen::Vector3d &position, Eigen::Vector3d &velocity, Eigen::Matrix3d &pose) const;
    void printState() const;
    void calibrateIMU()
    {
      _imu->init();
      _imu->calibrate();
    }
    void calibrateMag()
    {
      _imu->init();
      _imu->calibrateMag();
    }
  };
}; // namespace nanobot::sensor

#endif /* !SENSOR_SENSOR */