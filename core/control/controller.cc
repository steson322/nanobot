#include "controller.h"
#include <iostream>
#include <unistd.h>

#define I2C_PATH "/dev/i2c-1"
#define REF_CLOCK 25000000
#define SERVO_PWM_HZ 50

#define STEER_CH 0
#define STEER_RANGE 0.008
#define STEER_CENTER 0.074

#define MOTOR_CH 1
#define MOTOR_MIN 0.075
#define MOTOR_MAX 0.10

#define PCA9685_IIC_ADDRESS 0x40

#define MODE1 0x00
#define MODE1_RESTART_MASK 0x80
#define MODE1_SLEEP_MASK 0x10

#define MODE2 0x01
#define MODE2_OUTDRV_MASK 0x04

#define ALLCALL_ADDRESS 0xE0

#define LED0_ON_L 0x06
#define LED0_ON_H 0x07
#define LED0_OFF_L 0x08
#define LED0_OFF_H 0x09

#define ALL_LED_ON_L 0xFA
#define ALL_LED_ON_H 0xFB
#define ALL_LED_OFF_L 0xFC
#define ALL_LED_OFF_H 0xFD

#define PRE_SCALE 0xFE

nanobot::control::Controller::Controller() : Controller(I2C_PATH)
{
}

nanobot::control::Controller::Controller(std::string i2c_path)
{
  // initiallize I2C.
  _i2c = std::make_shared<nanobot::common::I2C>(i2c_path);
}

bool nanobot::control::Controller::init()
{
  if (!_i2c->init())
  {
    std::cerr << "Controller failed to initialize." << std::endl;
    return false;
  }

  this->setFrequency(PCA9685_IIC_ADDRESS, SERVO_PWM_HZ);

  return true;
}

void nanobot::control::Controller::setFrequency(uint8_t address, int frequency)
{
  uint8_t mode1;
  _i2c->read(address, MODE1, mode1);
  _i2c->write(address, MODE1, mode1 | MODE1_SLEEP_MASK);

  uint8_t mode2;
  _i2c->read(address, MODE2, mode2);
  _i2c->write(address, MODE2, mode2 | MODE2_OUTDRV_MASK);

  uint16_t pre_scale = REF_CLOCK / (4096 * frequency) - 1;
  _i2c->write(address, PRE_SCALE, pre_scale & 0xFF);

  usleep(500);

  _i2c->read(address, MODE1, mode1);
  _i2c->write(address, MODE1, mode1 & ~MODE1_SLEEP_MASK | MODE1_RESTART_MASK);
  _i2c->read(address, MODE2, mode2);
  _i2c->write(address, MODE2, mode2 | MODE2_OUTDRV_MASK);
}

void nanobot::control::Controller::setSteer(float steer)
{
  float duty = STEER_CENTER - steer * STEER_RANGE;
  this->setPwm(STEER_CH, duty);
  
  std::cout << "Servo = " << steer << " duty = " << duty << std::endl;
}

void nanobot::control::Controller::releaseSteer()
{
  this->setChannel(STEER_CH, false);
}

void nanobot::control::Controller::setMotor(float throttle)
{
  float duty = MOTOR_MIN + throttle * (MOTOR_MAX - MOTOR_MIN);
  this->setPwm(MOTOR_CH, duty);
  
  std::cout << "Motor = " << throttle << " duty = " << duty << std::endl;
}

void nanobot::control::Controller::releaseMotor()
{
  this->setChannel(MOTOR_CH, false);
}

void nanobot::control::Controller::setPwm(uint8_t channel, float duty)
{
  uint8_t channel_offset = channel << 2;
  uint16_t value = duty * 4096;

  uint8_t buffer[2];
  buffer[0] = value & 0xFF;
  buffer[1] = (value & 0x0F00) >> 8;

  _i2c->write(PCA9685_IIC_ADDRESS, LED0_ON_H + channel_offset, 0);
  _i2c->write(PCA9685_IIC_ADDRESS, LED0_ON_L + channel_offset, 0);
  _i2c->write(PCA9685_IIC_ADDRESS, LED0_OFF_H + channel_offset, buffer[1]);
  _i2c->write(PCA9685_IIC_ADDRESS, LED0_OFF_L + channel_offset, buffer[0]);
}

void nanobot::control::Controller::setChannel(uint8_t channel, bool on)
{
  uint8_t channel_offset = channel << 2;

  _i2c->write(PCA9685_IIC_ADDRESS, LED0_ON_H + channel_offset, on ? 0x10 : 0x00);
  _i2c->write(PCA9685_IIC_ADDRESS, LED0_OFF_H + channel_offset, on ? 0x00 : 0x10);
}