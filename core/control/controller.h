#ifndef CONTROL_SERVO
#define CONTROL_SERVO

#include <stdlib.h>
#include <memory>
#include "../common/i2c.h"

namespace nanobot::control
{
  class Controller
  {
  private:
    std::shared_ptr<common::I2C> _i2c;
    void setFrequency(uint8_t address, int frequency);
    void setPwm(uint8_t channel, float duty);
    void setChannel(uint8_t channel, bool on);

  public:
    Controller();
    Controller(std::string path);
    bool init();
    void setSteer(float steer);
    void setMotor(float throttle);
    void releaseSteer();
    void releaseMotor();
  };
}; // namespace nanobot::control

#endif /* !CONTROL_SERVO */