# Download
http://eigen.tuxfamily.org/index.php?title=Main_Page

# Unpack
sudo tar -zxvf eigen-3.3.8.tar.gz

# Install
cd eigen-3.3.8/
sudo mkdir build
cd build
sudo cmake ..
sudo make -j7
sudo make install